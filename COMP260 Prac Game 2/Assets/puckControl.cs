﻿using UnityEngine;
using System.Collections;

public class puckControl : MonoBehaviour {

    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;
    private AudioSource audio;
    public Transform startingPos;
    private Rigidbody rigidbody;
    

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
	}

    public void ResetPosition()
    {
        rigidbody.MovePosition(startingPos.position);
        rigidbody.velocity = Vector3.zero;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    {

        Debug.Log("Collision OnCollisionEnter: " + collision.gameObject.name);
        audio.PlayOneShot(wallCollideClip);

        if (paddleLayer.Contains(collision.gameObject))
        {
            audio.PlayOneShot(paddleCollideClip);
        }
        else
        {
            audio.PlayOneShot(wallCollideClip);
        }
        }
    

    void OnCollisionStay(Collision collision)
    {
        Debug.Log("Collision Stay: " + collision.gameObject.name);
    }

    void OnCollisionExit(Collision collision)
    {
        Debug.Log("Collision Exit: " + collision.gameObject.name);
    }
}
